# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

FROM bats/bats:latest

WORKDIR /opt/bum
COPY configure.ac .

RUN apk add \
    coreutils \
    git \
    zsh \
    make \
    vim \
    autoconf \
    automake \
    # Usefull when testing
    mandoc \
    man-pages \
    git-doc

COPY Makefile.am .
COPY src ./src

RUN <<OEF
autoreconf -i
mkdir build
cd build
../configure
make
make install
OEF

COPY . .

CMD [ "--tap", "t" ]
