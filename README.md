master branch:
![build status](https://gitlab.com/waterkip/bum/badges/master/pipeline.svg)

development branch:
![build status](https://gitlab.com/waterkip/bum/badges/development/pipeline.svg)

# bum

Welcome to bum! Bum is a collection of scripts and aliases that make working
with git from the command line easier and more fun (yes, git is fun!).

# Getting started

When you start with `bum` you will first need to execute the following
commands:

```
git clone git@gitlab.com/waterkip/bum.git
cd bum
autoreconf -i
mkdir build
cd build
../configure
make
sudo make install

# Bum is now installed in /usr/local/bin
# Add it to your PATH if you don't have it yet
export PATH=$PATH:/usr/local/bin
```

Bum comes with a set of aliases which are opinionated. You can either have base
aliases which are used by bum by including:

```
git config --global --add include.path /usr/local/share/bum/bum-core.conf
```

or you can enjoy my opinion and use:


```
git config --global --add include.path /usr/local/share/bum/bum-all.conf
```

To mix and match is also fine.

## bum config items

The following config items are in use by bum.

### bum.branches.protected

This configuration item tells bum which branches should not be deleted.

```
git config [ --local ] bum.branches.protected master
git config [ --local ] --add bum.branches.protected development
```

If you don't set these you can disable warnings. Bum falls back to master, dev
and development.
```
git config [ --local ] bum.nowarning.branchesprotected true
```

### bum.upstream

This configuration item tells bum which upstreams you have or want bum to look
at. Order is important, first match wins.

```
git config [ --local ] bum.upstream upstream
git config [ --local ] --add bum.upstream origin
```

If you don't set these you can disable warnings. Bum falls back to upstream and
origin.
```
git config [ --local ] bum.nowarning.upstream true
```

### bum.issue.prefix

The default issue prefix that bum uses when you create a new branch.

### bum.issue.regexp

The regexp you want bum to check on, if the format doesn't match, you will get
a warning. Bum will create the branch for you.

### bum.issue.format

The format of where the issue is used in a commit message. This value can be
either title or body.

### bum.pushnew

Boolean value that will tell bum to push the new branch to your remote
(origin). Defaults to false.

# How did the project start?

The project started after I explained git things to a new colleague
because we had some time to spare. He told me to publish what I have
because it would make a whole lot of people happy. And I like happy
people :). So I moved my git-\* scripts and aliases to this project to
make it usable for more people than just me.

# Pull requests

Feel free to submit pull requests if you think new features are needed
or if you find bugs or think some things aren't user friendly. Please be
aware that I like propper commit messages, so please use the
`etc/default-commit-message` template when creating a commit. Also use the
`.editorconfig` to make sure we all play nice together when adding,
changing and removing code.

# See also

* [A medium post about my git workflow](https://medium.com/@waterkip/my-git-workflow-7f2c145c9d6d)
* [/r/git on Reddit](https://www.reddit.com/r/git)

# License

All the files in this repository are licensed under the MIT license.
