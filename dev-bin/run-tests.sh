# SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

docker build --pull -t bum:test .
docker run --rm bum:test
