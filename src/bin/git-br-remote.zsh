# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

remotes="$(git remote)"
if [ -n "$1" ]
then
    remotes=$(echo "$remotes" | grep "$1");
fi

remotes=$(echo $remotes|sed -e 's/ /|/g')
git br --remote | grep -E "^  ($remotes)/" | grep -v 'HEAD ->'

# vim: ft=sh
