#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

LIBDIR=$(dirname $0)/../lib
source $LIBDIR/utils.sh
source $LIBDIR/git.zsh

1=${1:-HEAD}

is_merge_commit $1 && log_commit $1 && exit $?

first_merge="$(get_first_merge $*)"

[ $? -ne 0 ] && git log $1^..HEAD && exit $?

log_commit $first_merge

# vim:ft=zsh
