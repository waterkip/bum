# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

ROOT_DIR=$(git dir)

GCTAGS="$ROOT_DIR/ctags"
GCTAGS_ALL=$GCTAGS-all
git ls-files --directory | grep -vw 'min.js' | grep -v '.gitignore' > $GCTAGS

if [ -f $GCTAGS_ALL ]
then
    for i in $(grep -v '^#' $GCTAGS_ALL)
    do
        find $i -type f | grep -vw '.git' | grep -vw 'min.js' >> $GCTAGS
    done
    grep -v '/\.' $GCTAGS > $GCTAGS.all
    mv $GCTAGS.all $GCTAGS
fi

ctags -R -L $GCTAGS 2>/dev/null
