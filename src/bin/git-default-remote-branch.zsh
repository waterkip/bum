#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

source $(dirname $0)/../lib/utils.sh

if [ -z "$*" ]
then
    for i in $(git remote)
    do
        get_default_remote_branch $i
        echo "$i/$BUM_DEFAULT_BRANCH"
    done
else
    for i in $@
    do
        is_remote $i
        if [ "$?" -eq 0 ]
        then
            get_default_remote_branch $i
            echo "$i/$BUM_DEFAULT_BRANCH"
        else
            echo "$i isn't a configured remote!" >&2
        fi
    done
fi
