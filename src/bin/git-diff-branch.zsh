# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

set -x

if [ -z "$1" ]
then
    branch=$(git show-ref --head --dereference HEAD | grep HEAD | grep -v refs | awk '{print $1}')
else
    branch=$1
fi

merge_base=$(git merge-base $(git upstream) $branch)
if [ "$merge_base" = "$branch" ]
then
    merge_base=$(git merge-base $(git upstream) "$branch^")
fi

git diff $merge_base...$branch

# vim:ft=zsh
