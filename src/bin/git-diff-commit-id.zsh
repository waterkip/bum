#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2022 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

set -e

if [ -z "$1" ] || [ -z "$2" ]
then
    echo "You must supply two commit ID's!" >&2
    exit 1
fi

get_patch_id() {
  echo "$(git diff $1^..$1 2>/dev/null | git patch-id)"
}

if [[ "$(get_patch_id $1)" == "$(get_patch_id $2)" ]]
then
  exit 0;
fi

exit 1;
