# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

cd $(git pwd)
git s --porcelain | grep -v '^??' | \
    sed -e 's/^...//' | \
    xargs -o -r ${VISUAL:-${EDITOR:-vim}}
