# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

git_equals() {
    local cur
    local remote

    if [ $# -eq 2 ]
    then
        cur=$1
        remote=$2
    elif [ $# -lt 2 ]
    then
        remote=$1
    fi

    [ -z "$remote" ] && remote=@{u}
    [ -z "$cur" ] && cur=@

    id=$(git rev-parse $cur)

    remote_id=$(git rev-parse $remote)

    if [ $VERBOSE -eq 1 ]
    then
        local remote_branch=$(git rev-parse --abbrev-ref $remote)
        local local_branch=$(git rev-parse --abbrev-ref $cur)

        printf "%s %s\n%s %s\n" $local_branch $id \
            $remote_branch $remote_id
    fi

    [ "$id" != "$remote_id" ] && exit 1

    git status --porcelain | grep -qE '^ ?(M|D)'
    if [ $? -eq 0 ]
    then
        echo "You are in sync with your tracking branch. You do have uncommited changes"
    fi
    exit 0
}

VERBOSE=0
while getopts "v" opts
do
    case $opts in
        v) VERBOSE=1;;
    esac
done

shift "$(($OPTIND - 1))"

git_equals $@

# vim: ft=sh
