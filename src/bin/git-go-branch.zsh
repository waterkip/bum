# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

if [ -z "$1" ]
then
    echo "No branch-ish name supplied"
    exit 64
fi

branches=$(git br | sed -e 's/^*//' -e 's/ \+//g' | grep -iE "$1")
if [ $? -ne 0 ]
then
    echo "No branch found with name $1";
    exit 1
fi
found=$(echo "$branches"| wc -l)

if [ "$found" -eq 1 ] && [ "$(git cur)" != "$branches" ]
then
    git co $branches
elif [ "$found" -gt 1 ]
then
    echo "$(echo "$branches" | sed -e 's/^ \+//')"
    exit 1
elif [ "$found" -eq 0 ]
then
    echo "No branch found with name $1"
    exit 1
fi
exit 0;

# vim: ft=sh
