# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

GIT_MSG_MD5="$1.md5"

[ ! -f "$GIT_MSG_MD5" ] && exit 0

md5sum --status -c "$GIT_MSG_MD5"
RC=$?
rm $GIT_MSG_MD5
[ $RC -ne 0 ] && exit 0
echo "Aborting commit due to empty commit message (by commit-msg hook)" 1>&2
exit 1

# vim: ft=sh
