#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

source $(dirname $0)/../lib/utils.zsh

upstream=$(git upstream 2>/dev/null);
[[ -z $upstream ]] && exit 0;

cur=$(git cur)

declare -a skip;
skip=($(git config --get-all "bum.branches.noMerge"))

array_contains "$cur" $skip && exit 0;

dir=$(git dir)
if [[ -f "$dir/refs/remotes/$upstream" ]]
then
  r=(${(@s:/:)upstream})
  r[1]=();
  upstream=${(j./.)${r}}
elif [[ ! -f "$dir/refs/heads/$upstream" ]]
then
  exit 0;
fi

[[ $upstream != $cur ]] && exit 0

git rm-merged

# vim: set ft=zsh
