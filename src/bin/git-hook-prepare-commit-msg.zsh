#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

GIT_MSG="$1"
GIT_MSG_MD5="$1.md5"

rm -f "$GIT_MSG_MD5"

# Allow fixup's to passthrough
head -1 $GIT_MSG | grep -q '^fixup!' && exit 0

if [[ -n $2 ]]
then
    # $2 can be message, template, squash or commit
    # we only trigger on message, template.
    case $2 in
        sqaush|commit) exit 0;;
    esac
fi


typeset -a PREFIXES
PREFIXES=($(git config --get-all bum.issue.prefix))

[[ -z $PREFIXES ]] && exit 0

REGEXP="${(j:|:)PREFIXES=}"

lineno=$(grep -niE "^($REGEXP)-.+" $GIT_MSG) && exit 0

CUR=$(git cur)
# The branch does not contain a wanted prefix, so we don't bother
echo $CUR | grep -qiE "^$REGEXP-.+" || exit 0

ISSUE=(${(@s:-:)CUR})
MAIN_PREFIX=$(git config --get bum.issue.prefix)
ISSUE="$MAIN_PREFIX-$ISSUE[2]"

FORMAT=$(git config --get bum.issue.format)

if [[ ${FORMAT:-title} = 'title' ]]
then
    FIRST_COMMENT="$(grep -m 1 -n '^#' $GIT_MSG| awk -F\: '{print $1}')"
    FIRST_EMPTY=$(grep -m 1 -Exn '[[:blank:]]*' $GIT_MSG| awk -F\: '{print $1}')
    FIRST_NON_COMMENT=$(grep -m 1  -vn '^#' $GIT_MSG| awk -F\: '{print $1}')

    if [[ $FIRST_EMPTY -eq $FIRST_NON_COMMENT ]]
    then
        if [[ $FIRST_EMPTY -eq 1 ]]
        then
          sed -i -e "0,/^$/{s/^$/$ISSUE: /}" "$GIT_MSG"
        else
          LNO=$((FIRST_EMPTY -1 ))
          sed -i -e "$LNO,/^$/{s/^$/$ISSUE: \n/}" "$GIT_MSG"
        fi
    else
        sed -i -e "$FIRST_NON_COMMENT,/^/{s/^\(.\+\)/$ISSUE: \1/}" "$GIT_MSG"
    fi

    [[ -n $2 ]] && [ $2 = 'message' ] && exit 0

    md5sum "$GIT_MSG" > "$GIT_MSG_MD5"
    exit 0;
fi

if [[ $FORMAT = 'body' ]]
then
    KEYWORD=$(git config --get bum.issue.keyword)

    grep -q "^$KEYWORD: " $GIT_MSG && exit 0

    LNO=$(grep  -vn '^#' $GIT_MSG| tail -1 | awk -F\: '{print $1}')
    SO=$(grep  -n 'Signed-off-by' $GIT_MSG| tail -1 | awk -F\: '{print $1}')

    if [[ $SO -eq $LNO ]]
    then
        LNO=$(( LNO - 2))
    elif [[ $SO -lt $LNO ]]
    then
        LNO=$(( SO - 2))
    fi

    [[ $LNO -le 1 ]] && KEYWORD="\n$KEYWORD" && LNO=1;

    sed -i -r -e "$LNO,$LNO{s/^(.*)$/\1\n$KEYWORD: $ISSUE\n/}" "$GIT_MSG"
fi

# vim: ft=zsh
