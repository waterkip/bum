#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

source $(dirname $0)/../lib/git.zsh

is_merge_commit $1
