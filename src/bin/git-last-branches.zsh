# SPDX-FileCopyrightText: 2022 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

set -e

GIT_ROOT=$(git rev-parse --show-toplevel)

DOT_GIT="$GIT_ROOT/.git"
R="$DOT_GIT/logs/refs/heads"

for i in $(git reflog | awk '/checkout:/ {print $6, $8}')
do
    # If found in local ref
    [ -f "$R/$i" ] && echo $i
done | sort -u
 
