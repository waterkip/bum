# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

branch=${1-$(git cur)}

ref="$(git path refs/heads/$branch)"
if [ ! -f $ref ]
then
  echo "'$branch' is not a branch!" >&2
  exit 1
fi

mb=$(git merge-base $branch@{u} $branch)

git sl $mb...$branch

# vim:ft=zsh
