# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

_commit=${1:-HEAD}
[ $# -gt 0 ] && shift;

git l $@ $_commit^..$_commit

# vim: ft=sh
