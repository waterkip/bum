# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

git ls-tree -r --name-only HEAD: $1 | while read filename; do
  end_date=$(git log --date=short -1 --format="%ad" -- $filename)
  begin_date=$(git log --reverse --date=short --format="%ad" -- $filename | head -1)

  echo "$filename: $begin_date $end_date"

done

# vim: ft=sh
