# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

. $(dirname $0)/../lib/utils.sh

usage() {
    echo "git new-branch <ticket> <description>" 1>&2
    exit 1;
}

get_issue_prefix() {

    local issue_prefix="$(git config --get bum.issue.prefix)"

    if [ -z "$issue_prefix" ]
    then
        config_warn "bum.issue.prefix" "bum.nowarning.issueprefix"
    fi
    echo "$issue_prefix";
}

get_issue_format() {
    local issue_format="$(git config --get bum.issue.regexp)"

    if [ -z "$issue_format" ]
    then
        config_warn "bum.issue.regexp" "bum.nowarning.issueregexp"
    fi
    echo "$issue_format";
}

generate_branch_name() {
    local ticket="$1"
    local name="$2"

    local issue_prefix=$(get_issue_prefix)
    local branch

    if [ -z "$issue_prefix" ]
    then
        branch="$(echo $ticket-$name)";
    else
        ticket=$(echo $ticket | eval sed -e "'s/^$issue_prefix*//i'")
        branch="$(echo $issue_prefix-$ticket-$name)";
    fi
    branch="$(echo $branch | sed -e 's/-\+/-/g' -e 's/-$//')";

    if [ $empty -eq 1 ]
    then
        echo "$branch"
        return 0
    fi
    local issue_format=$(get_issue_format)
    if [ -n "$issue_format" ]
        then
        echo "$branch" | grep -qE "$issue_format"
        if [ $? -ne 0 ]
        then
            log_warn "Issue does not match issue format: $issue_format"
        fi
    fi

    echo "$branch"

}

issue_prefix_branch_check() {

    local branch="$1"
    local issue_prefix=$(get_issue_prefix)

    echo $branch | eval grep -qw -E "'^${issue_format}'"

    [ $? -eq 0 ] && return 0

    log_warn "Issue does not match issue format: $issue_format"
}

assert_branch_name() {
    local name="$1"

    if [ -e "$(git path refs/heads/$name)" ]
    then
        echo "Branch '$name' already exists!"
        exit 1;
    fi
}

get_tracking_from_git() {
    local tracking="$1"

    if [ -z "$tracking" ]
    then
        get_upstreams
        for i in $BUM_UPSTREAMS
        do
            is_remote $i
            [ $? -eq 0 ] && remote=$i && break
        done
        get_default_remote_branch $remote
        tracking="$remote/$BUM_DEFAULT_BRANCH"
    fi

    local commit=$(git rev-parse -q --verify $tracking)
    [ -n "$commit" ] && echo $tracking && return 0;
    return 1
}

create_branch() {
    local tracking=$1
    local branch=$2

    git co -t "$tracking" -b "$branch"
    local rv=$?
    [ $rv -ne 0 ] && exit $rv;
    git pull --rebase -q

    push_branch=$(git config --bool --get bum.pushnew)
    if [ "$push_branch" = 'true' ]
    then
        git po -q "$branch:$branch" --no-verify
    fi

    set_push_default_origin
}

set_push_default_origin() {
    [ -n $(git config --get remote.pushDefault) ] && return 0;

    log_warn "You don't have remote.pushDefault set, defaulting to 'origin'"
    git config --local --set remote.pushDefault origin
    return 0;
}

ticket=""
name=""
empty=0;

while getopts 't:e' name
do
    case $name in
        t) tracking=$OPTARG;;
        e) empty=1;;
    esac
done

shift $((OPTIND - 1))

if [ $empty -eq 0 ] && [ $# -lt 2 ]
then
    usage
fi

ticket="$1"
shift;

if [ $empty -eq 0 ] && [ -z "$ticket" ] || [ -z "$name" ]
then
    usage
elif [ -z "$name" ]
then
    usage
fi

name=$(echo $@ | sed -e 's/ \+/_/g')

branch_name="$(generate_branch_name $ticket $name)";

assert_branch_name "$branch_name"
tracking=$(get_tracking_from_git "$tracking")
[ $? -ne 0 ] && exit 1
create_branch "$tracking" "$branch_name"
exit $?

# vim: ft=sh
