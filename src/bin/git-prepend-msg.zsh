# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

prepend="$1"
[ -z "$prepend" ] && exit 1

subject=$(git title)
first_word=$(echo $subject | cut -d" " -f1)

echo $first_word | grep -q "$prepend:" && exit 0

echo $first_word| grep -q -- '-'
if [ $? -eq 0 ]
then
    prefix=$(echo $first_word | cut -d\- -f1)
fi

if [ -n "$prefix" ]
then
    echo "$prepend" | grep -iq "^$prefix-"
    if [ $? -eq 0 ]
    then
        subject=$(echo $subject | sed "s/^$first_word/$prepend:/")
    else
        subject=$(echo $subject | sed "s/^/$prepend: /")
    fi
else
    subject=$(echo $subject | sed "s/^/$prepend: /")
fi


COMMIT_MSG="$(git path BUM_COMMIT_MSG)"

git log --format="%B" -n1 > "$COMMIT_MSG"
sed -i "1 s#^.*#$subject#" "$COMMIT_MSG"
git commit --allow-empty --amend --file "$COMMIT_MSG"
rm -f "$COMMIT_MSG"
