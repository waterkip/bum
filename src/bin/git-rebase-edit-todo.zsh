#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2022 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

source $(dirname $0)/../lib/git.zsh
merge_while_rebase_branches $*
