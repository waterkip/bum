#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2022 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

# No autosquash here, because we can't detect it yet
GIT_SEQUENCE_EDITOR="git rebase-edit-todo $@" git rebase -i @{u}
