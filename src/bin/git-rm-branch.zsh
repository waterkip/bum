#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

. $(dirname $0)/../lib/utils.sh

if [ $# -lt 1 ]
then
    echo "git rm-branch <branch>" >&2
    exit 1
fi

cur=$(git cur)

for i in $@
do

    echo $i | grep -q "/"
    if [ $? -eq 0 ]
    then
        remote_branch=("${(@s|/|)i}")
        remote=$remote_branch[1]
        branch=$remote_branch[2]

        if [ -z "$branch" ]
        then
            log_warn "Will not do anything with '$i', no branch given"
            continue
        fi

        is_remote $remote
        if [ $? -ne 0 ]
        then
            log_warn \
                "Unable to delete branch from remote $remote, it does not exist!"
            continue
        fi
    else
        branch=$i
        remote=origin
    fi

    is_protected_branch $branch
    if [ $? -eq 0 ]
    then
        log_warn "Skipping protected branch $branch"
        continue
    fi

    if [ "$cur" = "$branch" ]
    then
        git co master -q
    fi

    git f $remote -q --prune
    git br-remote $remote | grep -qw $branch
    [ $? -eq 0 ] && git push $remote :$branch --no-verify

    git br | grep -qw $branch
    [ $? -eq 0 ] && git br -D $branch

done

exit 0;

# vim: ft=sh
