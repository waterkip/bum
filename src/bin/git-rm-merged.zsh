# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

. $(dirname $0)/../lib/utils.sh

branch=${1:-$(git upstream)}

git branch --merged $branch | awk '{print $NF}' \
    | grep -v "^$(git cur)$" | grep -v "^${branch}$" | xargs -r git rm-branch

# vim: ft=sh
