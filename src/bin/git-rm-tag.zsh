#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

tags=$(git tag)
git f origin

for i in $@
do

    echo $i | grep -q "/"
    if [ $? -eq 0 ]
    then
        remote_tag=("${(@s|/|)i}")
        remote=$remote_tag[1]
        tag=$remote_tag[2]

        if [ -z "$tag" ]
        then
            echo "Will not do anything with '$i', no tag given" 1>&2
            continue
        fi
    else
        tag=$i
        remote=origin
    fi

    ref_tag="$(git ls-remote -t $remote $tag)"
    if [ -n $ref_tag ]
    then
        git push -q $remote :refs/tags/$tag --no-verify
    fi

    echo $tags | eval grep -Eqw "'^$tag$'"
    if [ $? -eq 0 ]
    then
        git tag -d $tag
    fi
done

# vim: ft=sh
