# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

files="$(git status --porcelain | grep '^??' | sed -e 's/^?? //')"
if [ -z "$files" ];
then
    echo "No untracked files"
    exit 0;
fi

echo "We do not remove files just yet, be cautios about spaces in file names"
echo -e "$files"

#git status --porcelain | grep '^??' | sed -e 's/^?? //' | xargs rm -rf

# vim: ft=sh
