# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

. $(dirname $0)/../lib/utils.sh

get_upstreams
upstream=$(git upstream)

_set_master() {
    up=$1/$2
    [ "$upstream" = $up ] && exit 0;
    git upstream $up && exit 0
}

for i in $BUM_UPSTREAMS
do
    is_remote $i
    [ $? -ne 0 ] && continue

    get_default_remote_branch $i
    _set_master $i $BUM_DEFAULT_BRANCH
done

# vim: ft=sh
