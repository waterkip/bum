# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

git config --get-regexp ^alias\. | sed -e s/^alias\.// | awk '{printf "%15s =", $1} { $1=""; print $0 }'

# vim: ft=sh
