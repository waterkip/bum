# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

set -e

git_all() {
  git $@
  git submodule foreach "git $@"
}

if [ -z "$*" ]
then
  git
  exit $?
fi

git_all $@

# vim: ft=sh
