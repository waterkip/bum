# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

git_stash_options() {
    read -p '(s)how / (a)pply / (p)op / (d)rop stash / (n)ext / (q)uit: ' action < /dev/tty

    case $action in
        s) git stash show -p "$stash"; echo "" ;return 1;;
        a) git stash apply -q "$stash";;
        p) git stash pop -q "$stash";;
        d) git stash drop -q "$stash";;
        q)  echo ""
            echo Leaving stash "$stash" in place
            echo ""
            exit 0;;
        n) return 0;;
        *) return 1;;
    esac
    return 0;
}

# Reverse the order, because otherwise we need to start using an index
# in order to keep have a correct reference to a stash.
# Eg. have two stashes, stash{0} and stash{1}
# drop stash{0}, stash{1} now became stash{0}
# In our loop we don't have this, because we have the old value,
# stash{1}
#
# Luckely stashed are ordered on newest first:
#
# $ git stashed
# stash@{0}: c91438cec4 [1 second ago] WIP on master: xxx
# stash@{1}: bfeaee96b3 [3 minutes ago] WIP on master: xxx
#
# So we apply the oldest stash first, before moving to the newest one.
# This keeps me happy, no need for an additional index and we apply the
# oldest stuf first which seems to be the best option IMO.
# Hopefully tho, with this new shiney function, our stash stays
# relatively empty

for stash in $(git stash list | grep $(git cur) | cut -d ":" -f 1| tac)
do
    cat <<OEF
Found stash for current branch/commit:

$(git stash show "$stash")

OEF
    git_stash_options
    RV=$?
    while [ $RV -eq 1 ]
    do
        git_stash_options
        RV=$?
    done;
done


