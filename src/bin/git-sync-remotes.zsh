# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

. $(dirname $0)/../lib/utils.sh

do_git() {
    git $@
}

checkout_branch() {
    local branch="$1"
    local remote_branch="$2"

    git br | grep -qw "$branch"
    if [ $? -eq 0 ]
    then
        git co -q "$branch"
        [ $? -eq 0 ] && return
        echo "Unable to checkout branch '$branch'"
        return
    fi
    git co -q -t "$remote_branch" -b "$branch"
    [ $? -eq 0 ] && return
    echo "Unable to checkout remote branch '$remote_branch' as '$branch'"
}

sync_remote_branch() {
    local origin="$1"
    local branch=$(git cur)

    local remote_branch="$origin/$branch"

    git br-remote $origin | grep -qw "$remote_branch"
    if [ $? -ne 0 ]
    then
        echo "Remote does not exist for branch '$branch'!"

        local cur_remote=$(git upstream)
        if [ "$cur_remote" != 'zs/preprod' ] && [ $cur_remote != 'zs/production' ]
        then
            git set-master
        fi

        git rebase

        if [ $? -ne 0 ]
        then
            git rebase --abort
            git po HEAD --no-verify
            return 0;
        fi

        local remote=$(git upstream)
        if [ $cur_remote != $remote ]
        then
            git upstream $cur_remote
        fi

        git eq $cur_remote
        [ $? -ne 0 ] && return 0;

        return 1;
    fi

    git eq "$remote_branch"
    [ $? -eq 0 ] && return 0

    git pull --rebase -q $origin "$branch"
    [ $? -eq 0 ] && return 0

    _handle_failed_sync remote "Unable to rebase remote on itself"
    return 1
}

_handle_failed_sync() {
    local mode=$1; shift;
    git rebase --abort
    echo "$*" >&2;
    _add_failed_sync $mode
}

_add_failed_sync() {
    local mode=$1;

    local file=.rebase-$mode;
    touch $file
    git add $file
    git cm "Rebase $mode failed" $file
}

sync_upstream() {
    do_git pull -q --rebase
    [ $? -eq 0 ] && return 0;

    _handle_failed_sync master "Unable to rebase '$(git cur)' on itself"
    return 1;
}

sync_branch() {
    local origin=$1
    local br=$2;

    echo "Synching branch $br"

    local remote_branch="$origin/$br"

    checkout_branch "$br" "$remote_branch"

    sync_remote_branch $origin
    [ $? -ne 0 ] && return 1

    # Previous master run failed, skip it
    if [ -f .rebase-master ]
    then
        git reset --hard HEAD^
        return 1;
    fi

    git set-master

    sync_upstream
    if [ $? -ne 0 ]
    then
        echo "Pushing failed rebased master branch of $br"
        do_git push -q $origin --force --no-verify HEAD:$br
        return 1
    fi

    # equals upstream, candidate for removal
    git eq
    [ $? -eq 0 ] && return 0

    git eq "$remote_branch"
    [ $? -eq 0 ] && return 0
    echo "Pushing rebased version of $br"
    do_git push -q $origin --force --no-verify HEAD:$br
    return $?
}

git fat
git fo --prune
foo="$(git br-remote origin && git br)"

for br in $(echo "$foo" | sed -e 's/^*/ /' -e 's/origin\///g' | sort -ur)
do
    is_protected_branch $br
    if [ $? -eq 0 ]
    then
        log_warn "Skipping protected branch $br"
        continue
    fi

    case $br in
        SupportApp) continue;;
        *) sync_branch origin $br;;
    esac
    git co -q master

done

git rm-merged

# vim: ft=sh
