# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

_upstream=$(git rev-parse --abbrev-ref @{u} 2>/dev/null)
if [ $# -lt 1 ]
then
    if [ -z "$_upstream" ]
    then
        echo "No upstream found for $(git cur)!" >&2
        exit 2;
    else
        echo "$_upstream"
    fi
else
    if [ "$1" != "$_upstream" ]
    then
        git br --remote | grep -qE "\b$1\$"
        [ $? -ne 0 ] && exit 1
        git br --set-upstream-to=$1
    fi
fi

# vim: ft=sh
