# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

changes=$(git s --porcelain | grep -v '^??' | head -1)
[ -z "$changes" ] && exit 0

comment="Quick commit"
if [ -n "$*" ];
then
    comment="$@"
fi

git c --all -m "WIP: $comment" --no-verify

# vim: ft=sh
