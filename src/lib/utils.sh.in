#!/usr/bin/env sh
# SPDX-FileCopyrightText: 2019 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT
#
# Be advised that this script should ONLY support bash/sh as it is used by Bats
# you cannot use zsh specifics here. If you need zsh function: lib/utils.zsh is
# your friend.

BUM_VERSION=2.1

BUM_REMOTES=""
BUM_PROTECTED_BRANCHES=""
BUM_UPSTREAMS=""

log_warn() {
    echo "$@" 1>&2
}

is_remote() {
    remote=$1

    [ -d "$(git path refs/remotes/$remote)" ] && return 0
    return 1;
}

config_warn() {
    no_warning="$(git config --bool --get $2)"

    if [ "$no_warning" != 'true' ]
    then
        log_warn "You don't have $1 set, to disable this message run:"
        log_warn "git config [--local] $2 true"
    fi
}

get_protected_branches() {
    [ -n "$BUM_PROTECTED_BRANCHES" ] && return

    BUM_PROTECTED_BRANCHES="$(git config --get-all bum.branches.protected)"
    local warn=${1:-1}

    if [ -z "$BUM_PROTECTED_BRANCHES"  ]
    then
        config_warn "bum.branches.protected" "bum.nowarning.branchesprotected"
        log_warn "Automaticly protecting master, development and dev"
        BUM_PROTECTED_BRANCHES=$(printf "%s\n%s\n%s" master development dev)
    fi
}

is_protected_branch() {
    local remote_re=$1
    [ -z "$BUM_PROTECTED_BRANCHES" ] && get_protected_branches
    echo "$BUM_PROTECTED_BRANCHES" | grep -qw -E "^($remote_re)$"
}

get_default_remote_branch() {
    local file="$(git path refs/remotes/$1/HEAD)"
    [ ! -f "$file" ] && git remote set-head $1 --auto >/dev/null
    BUM_DEFAULT_BRANCH=$(awk -F/ '{print $NF}' < "$file")
}

is_upstream() {
    [ -z "$BUM_UPSTREAMS" ] && get_upstreams
    for i in $BUM_REMOTES
    do
        is_remote "$1" && echo $i && return 0
    done
    return 1
}

get_upstreams() {
    [ -n "$BUM_UPSTREAMS" ] && return

    BUM_UPSTREAMS="$(git config --get-all bum.upstream)"
    if [ -z "$BUM_UPSTREAMS" ]
    then
        config_warn "bum.upstream" "bum.nowarning.upstream"
        log_warn "Setting default upstreams to upstream and origin"
        BUM_UPSTREAMS='upstream origin'
    fi
}

# vim: ft=sh
