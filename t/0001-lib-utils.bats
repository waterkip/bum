# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

. /usr/local/libexec/bum/utils.sh

@test "test log_warn" {

    run log_warn "foo/bar"

    [ "$status" -eq 0 ]
    [ "$output" = "foo/bar" ]
}

@test "test config_warn" {

    setting="bum.foo"
    nowarn="bum.foo.warn"

    local want=$(cat <<OEF
You don't have $setting set, to disable this message run:
git config [--local] $nowarn true
OEF
)

    run config_warn $setting $nowarn

    [ "$status" -eq 0 ]
    [ "$output" = "$want" ]
}

@test "test is_remote" {

    run is_remote nope
    [ $status -eq 1 ]

    run is_remote origin
    [ $status -eq 0 ]

}

@test "test is_protected_branch" {

    run is_protected_branch foo
    [ $status -eq 1 ]

    run is_protected_branch master
    [ $status -eq 0 ]

}

@test "test get_upstreams" {

    run get_upstreams
    [ $status -eq 0 ]

    want=$(cat <<OEF
You don't have bum.upstream set, to disable this message run:
git config [--local] bum.nowarning.upstream true
Setting default upstreams to upstream and origin
OEF
)

    [ "$output" = "$want" ]

    BUM_UPSTREAMS=foo
    run get_upstreams
    [ $status -eq 0 ]
    [ "$output" = "" ]
}

# vim: ft=bash
