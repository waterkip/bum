# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-br-remote" {

    git co -b foo
    git cm "Empty commit" --allow-empty
    git po HEAD

    run git br-remote
    local want=$(cat <<OEF
  origin/foo
  origin/master
  upstream/master
OEF
)
    [ $status -eq 0 ]
    [ "$output" = "$want" ]

    # No upstream branches found
    run git br-remote bar
    [ $status -eq 1 ]
}

# vim: ft=bash
