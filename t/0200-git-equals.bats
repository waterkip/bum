# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-equals" {

    run git equals
    [ $status -eq 0 ]

    git cm "Empty commit" --allow-empty
    run git equals upstream/master
    [ $status -eq 1 ]

    # both remotes are equal
    run git equals upstream/master origin/master
    [ $status -eq 0 ]

    git po
    run git equals origin/master
    [ $status -eq 0 ]

    # both remotes are not equal
    run git equals upstream/master origin/master
    [ $status -eq 1 ]

}

# vim: ft=bash
