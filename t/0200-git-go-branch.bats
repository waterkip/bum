# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-go-branch" {

    run git go-branch
    [ $status -eq 64 ]

    run git go-branch foo
    [ $status -eq 1 ]
    [ "$output" = 'No branch found with name foo' ]

    git co -b foo-bar-baz
    run git go-branch foo
    [ $status -eq 0 ]

    run git cur
    [ $output = 'foo-bar-baz' ]

    # list all the branches
    git co -b foo-bar
    run git go-branch foo
    local want=$(cat <<OEF
foo-bar
foo-bar-baz
OEF
)
    [ $status -eq 1 ]
    [ "$output" = "$want" ]


}
# vim: ft=bash
