# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-log-commit" {

    skip

    run git-log-commit
    [ $status -eq 0 ]

}
# vim: ft=bash
