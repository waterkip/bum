# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-modif-time" {

    GIT_COMMITTER_DATE="Wed Feb 16 14:00 2011 +0100"
    git commit --amend --date="$GIT_COMMITTER_DATE" --no-edit -q
    unset GIT_COMMITTER_DATE

    echo "bar" > foo
    git add foo
    git_commit "add bar to foo" foo

    date="$(date +%Y-%m-%d)"
    run git-modif-time
    [ $status -eq 0 ]
    [ "$output" = "foo: 2011-02-16 $date" ]

}
# vim: ft=bash
