# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-new-branch" {

    run git new-branch foo
    [ $status -eq 1 ]
    [ "$output" = "git new-branch <ticket> <description>" ]

    # all them warns
    run git new-branch foo bar
    [ $status -eq 0 ]

    local want=$(cat<<OEF
You don't have bum.issue.prefix set, to disable this message run:
git config [--local] bum.nowarning.issueprefix true
You don't have bum.issue.regexp set, to disable this message run:
git config [--local] bum.nowarning.issueregexp true
You don't have bum.upstream set, to disable this message run:
git config [--local] bum.nowarning.upstream true
Setting default upstreams to upstream and origin
branch 'foo-bar' set up to track 'upstream/master'.
Switched to a new branch 'foo-bar'
OEF
);
    [ "$output" = "$want" ]

    run git cur
    [ "$output" = "foo-bar" ]

    git config --local bum.nowarning.upstream true
    git config --local bum.nowarning.issueprefix true
    git config --local bum.nowarning.issueregexp true

    want=$(cat<<OEF
Setting default upstreams to upstream and origin
branch 'foo-bar_baz' set up to track 'upstream/master'.
Switched to a new branch 'foo-bar_baz'
OEF
);
    run git new-branch foo bar baz
    [ $status -eq 0 ]

    [ "$output" = "$want" ]

    git config --replace-all --local bum.nowarning.upstream false
    git config --replace-all --local bum.nowarning.issueprefix false
    git config --replace-all --local bum.nowarning.issueregex false

    git config --local --add bum.upstream origin
    git config --local --add bum.issue.prefix testsuite
    git config --local --add bum.issue.regexp testsuite-[0-9]+

    run git new-branch 1234 bar baz

    [ $status -eq 0 ]

    want=$(cat<<OEF
branch 'testsuite-1234-bar_baz' set up to track 'origin/master'.
Switched to a new branch 'testsuite-1234-bar_baz'
OEF
);

    [ "$output" = "$want" ]

    run git new-branch testsuite-1234 bar baz da
    [ $status -eq 0 ]

    want=$(cat<<OEF
branch 'testsuite-1234-bar_baz_da' set up to track 'origin/master'.
Switched to a new branch 'testsuite-1234-bar_baz_da'
OEF
);
    [ "$output" = "$want" ]

    run git new-branch foobar baz
    [ $status -eq 0 ]

    want=$(cat<<OEF
Issue does not match issue format: testsuite-[0-9]+
branch 'testsuite-foobar-baz' set up to track 'origin/master'.
Switched to a new branch 'testsuite-foobar-baz'
OEF
);

    [ "$output" = "$want" ]

    run git new-branch foobar baz
    [ $status -eq 1 ]

    want=$(cat<<OEF
Issue does not match issue format: testsuite-[0-9]+
Branch 'testsuite-foobar-baz' already exists!
OEF
);

    [ "$output" = "$want" ]

}
# vim: ft=bash
