# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-rebase-merge" {

    git checkout master
    git config advice.skippedCherryPicks false
    echo "Master A" > ma
    git add ma
    git commit -m "Master A" --date="Wed Feb 16 14:00 2011 +0100"

    echo "Master b" > mb
    git add mb
    git commit -m "Master B" --date="Wed Feb 16 14:01 2011 +0100"

    echo "Master c" > mc
    git add mc
    git commit -m "Master C" --date="Wed Feb 16 14:02 2011 +0100"

    git po

    commit_c=$(get_last_commit_id);

    git_new_branch devel
    echo "I stay" > stay
    git add stay
    git commit -m  "Devel stays" --date="Wed Feb 16 15:01 2011 +0100"

    echo "I get removed" > removed
    git add removed
    git commit -m  "Devel removed" --date="Wed Feb 16 15:01 2011 +0100"
    get_removed=$(get_last_commit_id);

    echo "Merging us" > merged
    git add merged
    git commit -m  "Merged into master, loses at rebase" --date="Wed Feb 16 16:42 2011 +0100"
    just_merged=$(get_last_commit_id);

    git_new_branch feat
    echo "Feat A" > featA
    git add featA
    git commit -m  "Feature A" --date="Wed Feb 12 14:00 2011 +0100"
    echo "Feat B" >> featB
    git add featB
    git commit -m  "Feature B" --date="Wed Feb 16 14:01 2011 +0100"
    echo "Feat C" >> featC
    git add featC
    git commit -m  "Feature C" --date="Wed Feb 19 14:02 2011 +0100"

    git_new_branch bug
    echo "bug a" > buga
    git add buga
    git commit -m  "Bug A" --date="Wed Feb 13 14:00 2011 +0100"
    echo "Bug b" > bugb
    git add bugb
    git commit -m  "Bug B" --date="Wed Feb 14 14:00 2011 +0100"

    git_new_branch nono
    echo "I get removed" > removed
    git add removed
    git commit -m  "Nono removes devel" --date="Wed Feb 18 15:01 2011 +0100"

    git_new_branch just-merged
    echo "Merging us" > merged
    git add merged
    git commit -m "This will be merged instead of Just merged A" \
        --date="Wed Feb 18 15:01 2011 +0100"

    git checkout master
    git merge just-merged
    git po

    git checkout devel
    go_back_commit=$(get_last_commit_id)

    run git-rebase-merge
    [ $status = 0 ]
    run git log --format="%s"
    expect=$(cat <<OEF
Devel removed
Devel stays
This will be merged instead of Just merged A
Master C
Master B
Master A
Initial import
OEF
)
    [ "$output" = "$expect" ]

    git reset --hard $go_back_commit

    run git-rebase-merge bug
    [ $status = 0 ]

    run git log --topo-order --format="%s"
    expect=$(cat <<OEF
Devel removed
Devel stays
Bug B
Bug A
This will be merged instead of Just merged A
Master C
Master B
Master A
Initial import
OEF
)
    [ "$output" = "$expect" ]

    git reset --hard $go_back_commit

    run git-rebase-merge feat nono bug
    [ $status = 0 ]
    run git log --format="%s"
    expect=$(cat <<OEF
Devel stays
Nono removes devel
Feature C
Feature B
Feature A
Bug B
Bug A
This will be merged instead of Just merged A
Master C
Master B
Master A
Initial import
OEF
)
    [ "$output" = "$expect" ]


}
# vim: ft=bash
