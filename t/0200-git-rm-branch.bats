# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-rm-branch" {

    run git rm-branch
    [ $status -eq 1 ]
    [ "$output" = "git rm-branch <branch>" ]

    run git rm-branch master
    [ $status -eq 0 ]

    local want=$(cat<<OEF
You don't have bum.branches.protected set, to disable this message run:
git config [--local] bum.nowarning.branchesprotected true
Automaticly protecting master, development and dev
Skipping protected branch master
OEF
)
    [ "$output" = "$want" ]

    git config --local --bool bum.nowarning.branchesprotected 1
    run git rm-branch master
    [ $status -eq 0 ]
    local want=$(cat<<OEF
Automaticly protecting master, development and dev
Skipping protected branch master
OEF
)
    [ "$output" = "$want" ]

    git config --local --bool bum.nowarning.branchesprotected 0
    git config --local bum.branches.protected copy

    git co -b copy -q
    run git rm-branch copy
    [ $status -eq 0 ]
    [ "$output" = "Skipping protected branch copy" ]
    git config --local --replace-all bum.branches.protected master

    commit_id=$(git rev-parse --short @)
    run git rm-branch copy
    [ $status -eq 0 ]
    [ "$output" = "Deleted branch copy (was $commit_id)." ]

    # delete non-existing branch, you wanted it gone, it is gone
    run git rm-branch copy
    [ $status -eq 0 ]

    run git rm-branch foo/copy
    [ $status -eq 0 ]
    [ "$output" = 'Unable to delete branch from remote foo, it does not exist!' ]

    run git rm-branch origin/copy
    [ $status -eq 0 ]
}
# vim: ft=bash
