# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-rm-merged" {

    git_empty_commit
    git po master -q

    local commit_id=$(git rev-parse --short @)
    local head_1=$(git rev-parse --short @^)

    # Must be deleted
    git co -t origin/master -b foo -q
    git reset -q --hard $head_1

    # Must stay
    git co -t origin/master -b bar -q
    git_empty_commit

    # Must be deleted
    git co -t origin/master -b baz -q

    git config --local bum.branches.protected master
    git co -q master

    run git rm-merged
    [ $status -eq 0 ]
    local want="\
Deleted branch baz (was $commit_id).
Deleted branch foo (was $head_1)."

    [ "$output" = "$want" ]

    # Must be deleted
    git co -t origin/master -b foo -q
    git reset -q --hard $head_1
    git co master

    # Delete all branches which are merged into bar
    run git rm-merged bar
    [ $status -eq 0 ]
    local want=$(cat<<OEF
Deleted branch foo (was $head_1).
OEF
)
    local want="\
Deleted branch foo (was $head_1)."
    [ "$output" = "$want" ]


}
# vim: ft=bash
