# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-rm-tag" {

    # Setup
    git tag -am "Tagging foo" foo
    run git tag
    [ "$output" = "foo" ]

    git push origin --tags -q

    run git ls-remote -t origin foo
    [ -n "$output" ]

    run git rm-tag foo
    [ $status -eq 0 ]

    run git tag
    [ -z "$output" ]

    run git ls-remote -t origin foo
    [ -z "$output" ]

    run git rm-tag foo
    [ $status -eq 0 ]

    # Setup
    git tag -am "Tagging foo" foo
    git push upstream --tags
    run git tag
    [ "$output" = "foo" ]

    run git rm-tag upstream/foo
    [ $status -eq 0 ]

    run git tag
    [ -z "$output" ]

    run git ls-remote -t upstream foo
    [ -z "$output" ]

    run git rm-tag origin/foo
    [ $status -eq 0 ]
}
# vim: ft=bash
