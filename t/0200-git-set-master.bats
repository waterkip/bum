# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-set-master" {

    git config --local bum.upstream upstream

    run git-set-master
    [ $status -eq 0 ]
    [ "$output" = "branch 'master' set up to track 'upstream/master'." ]

    run git-set-master
    [ $status -eq 0 ]
    [ -z "$output" ]

    git config --local --unset-all bum.upstream upstream
    git config --local bum.nowarning.upstream true

    git upstream origin/master

    run git-set-master
    [ $status -eq 0 ]

    local want="$(cat <<OEF
Setting default upstreams to upstream and origin
branch 'master' set up to track 'upstream/master'.
OEF
)";
    [ "$output" = "$want" ]
}
# vim: ft=bash
