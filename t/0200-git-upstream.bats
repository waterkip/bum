# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-upstream" {

    run git-upstream
    [ $status -eq 0 ]

}
# vim: ft=bash
