# SPDX-FileCopyrightText: 2019 - 2023 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

load init-bum
bum_init

@test "git-wip" {

    # clean working dir
    run git-wip
    [ $status -eq 0 ]

    # Change foo
    echo 'bar' > foo
    run git wip
    [ $status -eq 0 ]

    run git l --format='%s' -n1
    [ "$output" = 'WIP: Quick commit' ]

    # Test custom WIP
    echo 'baz' > foo
    run git wip add bar to foo
    [ $status -eq 0 ]

    run git l --format='%s' -n1
    [ "$output" = 'WIP: add bar to foo' ]
}
# vim: ft=bash
