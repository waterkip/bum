# SPDX-FileCopyrightText: 2019 - 2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: MIT

diag() {
    echo "$@" | sed -e 's/^/# /' >&3 ;
}

git_commit() {
    comment="${1:-Testsuite commit}"
    shift
    git commit -q -m "$comment" $@
}

get_last_commit_id() {
    git rev-parse --short @
}

git_empty_commit() {
    comment="${1:-Empty commit}"
    git_commit "$comment" --allow-empty
}

git_sl() {
    git log --format='%h %aI %s %aE'
}

git_new_branch() {
    git co -t origin/master -b $1 -q
}

git_init() {
    local automated=1
    [ -n "$1" ] && automated=$1

    dir="$(mktemp -d)"
    origin_dir="$dir/origin"
    upstream_dir="$dir/upstream"
    local_dir="$dir/local"

    mkdir -p $origin_dir $upstream_dir $local_dir

    cd $upstream_dir
    git init -q --bare .

    cd "$origin_dir"
    git init -q --bare .

    cd "$local_dir"
    git init -q .
    git config --replace-all user.name "Bum Testsuite"
    git config --replace-all user.email "bum-testsuite@opperschaap.net"

    git config --replace-all include.path /usr/local/share/bum/bum-core.conf

    git remote add origin "$origin_dir"
    git remote add upstream "$upstream_dir"

    git fa -q
    echo "foo" > foo
    git add foo
    git cm "Initial import" -q

    git po -q master
    git push -q upstream master
    git br -q --set-upstream-to=origin/master
    if [ $automated -ne 1 ]
    then
        echo "$dir"
    fi
}


bum_init() {
    git_init &>/dev/null
}

